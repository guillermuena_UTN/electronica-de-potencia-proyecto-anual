#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif

#include <cr_section_macros.h>

#define _ADC_CHANNLE_1 ADC_CH5
#define _ADC_CHANNLE_2 ADC_CH4
#define _LPC_ADC_ID LPC_ADC
#define _LPC_ADC_IRQ ADC_IRQn

#define HB_PORT 1
#define HB_PIN	1

#define FB_PORT 1
#define FB_PIN	2

#define ALPHA_UP_PORT 1
#define ALPHA_UP_PIN  3

#define ALPHA_DOWN_PORT 1
#define ALPHA_DOWN_PIN  4

#define ALPHA_OUT_PORT 1
#define ALPHA_OUT_PIN  5

#define ALPHA_EN_PORT 1
#define ALPHA_EN_PIN  6

#define R1 1500
#define R2 3000

#define RELOAD_VALUE_ALPHA 20

#define vector_lenght 512

double vector_v[vector_lenght];
double vector_i[vector_lenght];
double vector_p[vector_lenght];
volatile uint32_t pos_v = 0;
volatile uint32_t pos_i = 0;

uint8_t fb_state = 0;
uint8_t hb_state = 0;
uint8_t alpha = 0;
uint8_t apply_alpha = false;
uint8_t alpha_counter = 0;
uint8_t state = false;

void ADC_IRQHandler(void)
{
	uint16_t dataADC;
	/* Interrupt mode: Call the stream interrupt handler */
	NVIC_DisableIRQ(_LPC_ADC_IRQ);
	if (Chip_ADC_ReadStatus(_LPC_ADC_ID, _ADC_CHANNLE_1, ADC_DR_DONE_STAT) != SET)
	{
		Chip_ADC_Int_SetChannelCmd(_LPC_ADC_ID, _ADC_CHANNLE_1, DISABLE);
		Chip_ADC_ReadValue(_LPC_ADC_ID, _ADC_CHANNLE_1, &dataADC);
		pos_v = pos_v % vector_lenght;
		vector_v[pos_v] = ((3.3/4096) * dataADC) * (220/9);
		pos_v++;
		Chip_ADC_Int_SetChannelCmd(_LPC_ADC_ID, _ADC_CHANNLE_1, ENABLE);
	}
	if (Chip_ADC_ReadStatus(_LPC_ADC_ID, _ADC_CHANNLE_2, ADC_DR_DONE_STAT) != SET)
	{
		Chip_ADC_Int_SetChannelCmd(_LPC_ADC_ID, _ADC_CHANNLE_2, DISABLE);
		Chip_ADC_ReadValue(_LPC_ADC_ID, _ADC_CHANNLE_2, &dataADC);
		pos_i = pos_i % vector_lenght;
		vector_i[pos_i] = ((dataADC*3.3)/4096) * (220/9);
		pos_i++;
		Chip_ADC_Int_SetChannelCmd(_LPC_ADC_ID, _ADC_CHANNLE_2, ENABLE);
	}
	NVIC_EnableIRQ(_LPC_ADC_IRQ);
}

void SysTick_Handler(void)
{
	if (Chip_GPIO_GetPinState(LPC_GPIO, HB_PORT, HB_PIN))
	{
		fb_state = 0;
		hb_state = 1;
	}
	if (Chip_GPIO_GetPinState(LPC_GPIO, FB_PORT, FB_PIN))
	{
		fb_state = 1;
		hb_state = 0;
	}
	if(Chip_GPIO_GetPinState(LPC_GPIO, ALPHA_UP_PORT, ALPHA_UP_PIN))
	{
		alpha += 10;
		alpha%101;
	}
	if(Chip_GPIO_GetPinState(LPC_GPIO, ALPHA_DOWN_PORT, ALPHA_DOWN_PIN))
	{
		alpha -= 10;
		if(alpha < 0)
		{
			alpha = 0;
		}
	}
	if(Chip_GPIO_GetPinState(LPC_GPIO, ALPHA_EN_PORT, ALPHA_EN_PIN))
	{
		apply_alpha = (apply_alpha) ? false : true;
		alpha_counter = uint8_t(RELOAD_VALUE_ALPHA * (alpha/100));
	}
	if (apply_alpha)
	{
		if (alpha_counter)
		{
			alpha_counter--;
		}
		else
		{
			alpha_counter = state ? uint8_t(RELOAD_VALUE_ALPHA * (1 - alpha/100)) : uint8_t(RELOAD_VALUE_ALPHA * (alpha/100));
			Chip_GPIO_WritePortBit(pGPIO, port, pin, !state);
			state = !state;
		}
	}
}

double calculate_mean_value(uint8_t sel)
{
	double accum = 0;
	if(sel == CURR)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			accum += (vector_i[i]/vector_lenght);
		}
	}
	if (sel == VOLT)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			accum += (vector_v[i]/vector_lenght);
		}
	}
	return accum;
}

double calculate_RMS_value(uint8_t sel)
{
	double accum = 0;
	if(sel == CURR)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			accum += ((vector_i[i]*vector_i[i])/vector_lenght);
		}
	}
	if (sel == VOLT)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			accum += ((vector_v[i]*vector_v[i])/vector_lenght);
		}
	}
	accum = sqrt(accum);
	return accum;
}

double calculate_p2p_value(uint8_t sel)
{
	double min = 0;
	double max = 0;
	if(sel == CURR)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			if (vector_i[i] > max) max = vector_i[i];
			if (vector_i[i] < min) min = vector_i[i];
 		}
	}
	if (sel == VOLT)
	{
		for(int i = 0; i < vector_lenght; i++)
		{
			if (vector_v[i] > max) max = vector_v[i];
			if (vector_v[i] < min) min = vector_v[i];
		}
	}
	return (max - min);
}

void calculate_power (void)
{
	for(int i = 0; i < vector_lenght; i++)
	{
		vector_p[i] = abs(vector_i[i])*abs(vector_v[i]);
	}
}

int main(void)
{
    SystemCoreClockUpdate();
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, HB_PORT, HB_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, FB_PORT, FB_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, ALPHA_DOWN_PORT, ALPHA_DOWN_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, ALPHA_UP_PORT, ALPHA_UP_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, ALPHA_EN_PORT, ALPHA_EN_PIN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, ALPHA_OUT_PORT, ALPHA_OUT_PIN);
    Chip_ADC_Init(_LPC_ADC_ID, &ADCSetup);
    Chip_ADC_EnableChannel(_LPC_ADC_ID, _ADC_CHANNLE_1, ENABLE);
    Chip_ADC_EnableChannel(_LPC_ADC_ID, _ADC_CHANNLE_2, ENABLE);
    SysTick_Config(SystemCoreClock/1000);

    while(1)
    {

    }
    return 0 ;
}
